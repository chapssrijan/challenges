<?php


/**
 * Takes associative array as an argument and returns new array that has locations as key containing location residents
 * in an array.
 *
 * @param array $personLocations
 *
 * @return array
 */
function formatArrayByLocation( array $personLocations ): array {
	return [];
}


$personLocations = [
	'Ilmari Ikonen'      => 'Myllykoski',
	'Villiam Sundqvist'  => 'Rauma',
	'Kusti Eerola'       => 'Helsinki',
	'Tarvo Rantanen'     => 'Joensuu',
	'Isto Palander'      => 'Helsinki',
	'Eriika Haapalainen' => 'Joensuu',
];

print_r( formatArrayByLocation( $personLocations ) );
